﻿/*  P03FactN.c
    Lee un numero "n"(>0 y <= 33) y obtiene el factorial.
    Tiene las caracteristicas:
    1) Puede ejecutarse las veces deseadas dentro del mismo proceso.
    2) Checa que "n" sea un valor valido.
    Erick Nieto Mart�nez
*/

#include<stdio.h>
#include<ctype.h>
int main()
{
    int num=0,ter=0;                                                  // Decalramos variables enteras o numericas
    float fac=1;                                                    //Declaramos la variable float porque tiene mayor capacidad de almacenamiento
    char sn=0;
                                                            //Vairable que almacenara la letra apretada para saber si reiniciar el programa o finalizarlo
    while(1)                                                        //Ciclo infinito
    {
        do{
            system("cls");                                          //Limpia pantalla
            printf("\nALIMENTAR EL NUMERO \"n\" (>0 Y <= 33):\t");  // Imprime indicaciones
            scanf("%d", &num);                                      //Permite al usuario meter algun valor numerico
        }while(!(num>0 && num<34));                                 //Condicion para saber si se repite ese ciclo o si continua con el programa
        for(ter=1,fac=1; ter<=num; ter++)                           //Ter empieza en uno para empezar a multiplicar desde uno y tambien se limpia fac para que cuando se requiera que nuevamente se
        {                                                           //quiera empezar de nuevo no se empiece a multipplicar desde el ultimo resultado de nuestra peticion anterior
            fac*=ter;                                               // fac = fac * ter; arreglo para la operacion de un factorial
            printf("\nTermino: %d\tResul: %g", ter,fac);            //formato %g general (flotante o cientifico/exponencial
        }
        printf("\n\n\tEL FACTORIAL ES: %f", fac);                   // Se imprime el resultado
        do{
            printf("\n\nOtro Proceso (s/n): ");                     //Pregunta si queremos volver a meter otro valor
            fflush(stdin);                                          //limpia el buffer de entrada. evita se imprima 2 veces.
            sn = getchar();                                         //scanf("%c", &sn);
            sn=tolower(sn);                                         //convierte a minusculas el contenido de la variable
        }while(!(sn=='s' || sn=='n'));                              //Si se presiona s o n se saldra de este ciclo
        if(sn=='n')break;                                           //Si se presion� anterior mente 'n' se acabara el programa
    }
    
    return 0;
}
